window.addEventListener('load', main);

/**
 * flips a single card (if coordinates valid)
 * 
 * @param {state} s 
 * @param {number} col 
 * @param {number} row 
 */
function flip1( s, col, row) {
  s.game.uncover(row, col);
}

/**
 * flip a card with given coordinate and its neigbors
 * 
 * @param {state} s 
 * @param {number} col 
 * @param {number} row 
 */
function flip( s, col, row) {
  flip1( s, col, row);
}

/**
 * creates enough cards for largest board (24 * 20)
 * registers callbacks for cards
 * 
 * @param {state} s 
 */
function prepare_dom(s) {
  const grid = document.querySelector(".grid");
  const nCards = 24 * 20 ; // max grid size
  for( let i = 0 ; i < nCards ; i ++) {
    const card = document.createElement("div");
    card.className = "card";
    card.setAttribute("data-cardInd", i);
    // Register click event to card
    card.addEventListener("click", () => {
      card_click_cb( s, card, i);
    });

    // Register Taphold event to card
    $(card).on("taphold", () => {
      $.event.special.tap.tapholdThreshold = 1000; // setting taphold time
      card_rightclick_cb( s, card, i);
    });

    // Register right-click event to card
    card.addEventListener("contextmenu", function(e) {
      e.preventDefault();
      card_rightclick_cb( s, card, i);
    }, false);

    //  Add a hover over effect to card
    $(card).hover(
      function() {
        $(this).addClass("hover");
      },
      function() {
        $(this).removeClass("hover");
      }
    ); 
    grid.appendChild(card);
  }
}

/**
 * updates DOM to reflect current state
 * - hides unnecessary cards by setting their display: none
 * - adds "flipped" class to cards that were flipped
 * 
 * @param {object} s 
 */

function render(s) {
  // Dictionary used to help render the numbers
  var proximity = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight"
  };

  const grid = document.querySelector(".grid");
  grid.style.gridTemplateColumns = `repeat(${s.cols}, 1fr)`;
  for( let i = 0 ; i < grid.children.length ; i ++) {
    const card = grid.children[i];
    const ind = Number(card.getAttribute("data-cardInd"));

    const grid_col = ind % s.cols; 
    const grid_row = Math.floor(ind / s.cols); 

    if( ind >= s.rows * s.cols) { // Hide card if its greater than row * col
      card.style.display = "none";
    }
    else {
      card.style.display = "block";
      if(s.restart){
        card.className = "card";
      }else if(s.game.exploded !== true){ // No explosion case
        if (s.moves === 1){ // This case handles the case where you set flags before clicking on the first hidden block. 
          if (s.game.arr[grid_row][grid_col].state === "hidden" && $(card).hasClass("marked")){ // Remarks the element
            s.game.mark(grid_row, grid_col);
            s.game.nmarked -=1;
          }else if (s.game.arr[grid_row][grid_col].state === "shown" && $(card).hasClass("marked")){ // If the preexisting flag is now shown then remove the flag
            s.game.nmarked -=1;
          }

        }

        if(s.game.arr[grid_row][grid_col].state === "shown"){ // If the card is shown
          card.className = "card"
          if (s.game.arr[grid_row][grid_col].mine) { // If the card is a mine, set the thing to a mine.
            $(card).unbind('mouseenter mouseleave');
            card.classList.add("bomb");
          }else if (s.game.arr[grid_row][grid_col].count !== 0){ // If the card has a number, set it to a number card
            $(card).unbind('mouseenter mouseleave');
            card.classList.add(proximity[s.game.arr[grid_row][grid_col].count]);
          }else{ // Default flip card
            $(card).unbind('mouseenter mouseleave');
            card.classList.add("shown");
          }
        }
        
        if(s.game.arr[grid_row][grid_col].state === "marked"){ // If the card is marked
          card.className = "card"
          card.classList.add("marked");
        }
      }else{
        // Exploded Case Render Everything    
        card.classList.className = "card";
        $(card).unbind('mouseenter mouseleave');
        // Sets cards back to default with effects
        $(card).hover(
          function() {
            $(this).addClass("hover");
          },
          function() {
            $(this).removeClass("hover");
          }
        ); 
        // Flip all the cards
        if (s.game.arr[grid_row][grid_col].mine) { // set to mine card
          card.classList.add("bomb");
        }else if (s.game.arr[grid_row][grid_col].count !== 0){ // set to number card
          card.classList.add(s.game.arr[grid_row][grid_col].count);
        }else{ // set to default card
          card.classList.add("shown");
        }
      }
    }
  }
}
let clickSound = new Audio("clunk.mp3");

/**
 * callback for clicking a card
 * - toggle surrounding elements
 * - check for winning condition
 * @param {state} s 
 * @param {HTMLElement} card_div 
 * @param {number} ind 
 */
function card_click_cb(s, card_div, ind) {
  const col = ind % s.cols;
  const row = Math.floor(ind / s.cols);

  
  if (!s.game.exploded){ 
    if(s.moves === 0){
      start(s); // Start timer at first move
    }
    if (s.game.arr[row][col].state === "hidden"){ // Flip card if the current state is hidden
      flip(s, col, row);
      s.moves ++;
      render(s); // Update board
      clickSound.play();
    }  
  }
  
  // Update flag count
  document.querySelectorAll(".flagCount").forEach(
    (e)=> {
      e.textContent = String((s.flags-s.game.nmarked));
    });

  // Win loss checker
  if (s.game.exploded){ // Lose overlay
    document.querySelectorAll("#overlay")[1].classList.toggle("active");
    stop(s);
  }else if (s.game.getStatus().done){ // Win overlay
    document.querySelectorAll("#overlay")[0].classList.toggle("active");
    document.querySelector(".winSeconds").textContent = s.t;
    stop(s);
  }
}

/**
 * callback for right-clicking a card
 * - toggle surrounding elements
 * - check for winning condition
 * @param {state} s 
 * @param {HTMLElement} card_div 
 * @param {number} ind 
 */
function card_rightclick_cb(s, card_div, ind) {
  const col = ind % s.cols;
  const row = Math.floor(ind / s.cols);

  // if the card is hidden and we have enough flags
  if (s.game.arr[row][col].state === "hidden" && s.game.nmarked < s.flags){
    s.game.mark(row,col);
    card_div.classList.toggle("marked");
    $(card_div).unbind('mouseenter mouseleave');
    $(card_div).removeClass('hover')
  }else if (s.game.arr[row][col].state === "marked"){ // unmarking a card
    s.game.mark(row,col);
    card_div.classList.toggle("marked");

    // adding animation back to card
    $(card_div).hover(
      function() {
        $(this).addClass("hover");
      },
      function() {
        $(this).removeClass("hover");
      }
    ); 
  }

  render(s);

  // Update the flag count text after marking
  document.querySelectorAll(".flagCount").forEach(
    (e)=> {
      e.textContent = String((s.flags-s.game.nmarked));
    });
}

// function to restart game
function restart(state) {
  state.game.init(state.rows, state.cols, state.bombs);
  state.restart = true;
  render(state);
  state.restart = false;
  state.moves = 0;
  stop(state);
  // Reset the text timer to '000'
  document.querySelector(".elapseTime").textContent = '000'; 
  state.t = 0;
  // Reset the mine count
  document.querySelectorAll(".flagCount").forEach(
    (e)=> {
      e.textContent = String((state.flags-state.game.nmarked));
    });
}

/**
 * callback for the top button
 * - set the state to the requested difficulty
 * - render the state
 * 
 * @param {state} s 
 * @param {string} difficulty
 */
function button_cb(s, difficulty) {

  s.cols = difficulty.cols;
  s.rows = difficulty.rows;
  s.bombs = difficulty.bombs;
  s.flags = difficulty.flags;

  restart(s, difficulty);
}

// Start timer function
function start(s) {
  s.timer = setInterval(function(){
    s.t++;
    document.querySelector(".elapseTime").textContent = ('000' + s.t).substr(-3); 
  }, 1000);  
}

// Stop timer function
function stop(s){
  if(s.timer) window.clearInterval(s.timer);
}

function main() {
  // create state object
  let state = {
    cols: null,
    rows: null,
    bombs: null,
    flags: null,
    moves: 0,
    restart: false,
    t: 0,
    timer: null,
    game: new MSGame()

  }

  // predefining difficulty levels
  // cols, rows, amount of bombs and amount of flags
  const difficulty = {
    easy: {
      cols:10,
      rows:8,
      bombs: parseInt(((10 * 8) * 15) / 100),
      flags: parseInt(((10 * 8) * 15) / 100)
    },
    medium: {
      cols:18,
      rows:14,
      bombs:parseInt(((18 * 14) * 20) / 100),
      flags:parseInt(((18 * 14) * 20) / 100)
    },
    hard: {
      cols:24,
      rows:20,
      bombs:parseInt(((24 * 20) * 25) / 100),
      flags:parseInt(((24 * 20) * 25) / 100)
    }
  } 

  
  // get browser dimensions - not used in thise code
  let html = document.querySelector("html");
  // console.log("Your render area:", html.clientWidth, "x", html.clientHeight)
  
  // register callbacks for buttons
  document.querySelectorAll(".menuButton").forEach((button) =>{
    mode = button.getAttribute("data-size");
    button.innerHTML = mode;
    button.addEventListener("click", button_cb.bind(null, state, difficulty[mode]));
  });

  // callback for overlay click - hide overlay and regenerate game
  document.querySelectorAll("#overlay").forEach((overlay) =>{
    overlay.addEventListener("click", () => {
      overlay.classList.remove("active");
      restart(state);
    });
  });

  // sound callback
  let soundButton = document.querySelector("#sound");
  soundButton.addEventListener("change", () => {
    clickSound.volume = soundButton.checked ? 0 : 1;
  });


  // create enough cards for largest game and register click callbacks
  prepare_dom( state);

  // simulate pressing 10x8 button to start new game
  button_cb(state, difficulty["easy"]);
}
